pi_list = {
    1 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    2 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    3 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    4 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    5 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    6 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    7 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    8 : {'mirror': 'ITMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    9 : {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    10: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    11: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    12: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    13: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    14: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    15: {'mirror': 'ITMY', 'damp_phase': 120 , 'damp_gain': 120000 , 'bias': -150},
    16: {'mirror': 'ITMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    17: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    18: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    19: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    20: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    21: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    22: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    23: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    24: {'mirror': 'ETMX', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    25: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    26: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    27: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    28: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    29: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    30: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    31: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''},
    32: {'mirror': 'ETMY', 'damp_phase': '', 'damp_gain': '', 'bias': ''}
}

####
# Finding the 'damp_phase' parameter:
# In SUS_PI.py guardian there is a function 'BPFilterPhaseEst'.
# 
# damp_phase  = np.mod(-BPFilterPhaseEst(1.4) + 60 , 360)
# 
# read as:
# proven good damping phase, at +1.4 Hz relative frequency from centre of PI bandpass, optimised at +60 phase (so the +60 phase filter was engaged)
# This is actually the phase of the REST of the loop to damp the PI. So the phase setting and the BP filter phase added together compensate eachtother. Hence 'damp_phase' is constant even if you move the BP filter, as long as you correct the centre frequency
#
# With PLL its similar, but the phase sign must be flipped because the phasor there works opposite to general logic. ie (the -1 in the phase corrects this):
# int(np.mod(-1*PLL_Phase_input -BPFilterPhaseEst(freqObserved - freqCentre), 360))
#
# Moreover, some modes have another bandpass. This is covered by the extraBPmodes, and this needs to be added over time as necessary.
# This info is needed by the extraBPFilterPhaseEst() function
####
extraBPmodes = { 1: {'centre':186.0},
                 3: {'centre':273.5},
                 6: {'centre':200.0},
                 8: {'centre':160.0},
                 9: {'centre':214.0},
                11: {'centre':272.0},
                12: {'centre':414.5},
                13: {'centre':427.5},
                14: {'centre':200.5},
                16: {'centre':158.0},
                17: {'centre':742.5},
                18: {'centre':276.0},
                19: {'centre':208.5},
                20: {'centre':429.25},
                21: {'centre':438.5},
                22: {'centre':218.0},
                24: {'centre':161.85},
                25: {'centre':206.5},
                26: {'centre':279.0},
                27: {'centre':740.0},
                28: {'centre':430.5},
                29: {'centre':437.8},
                30: {'centre':215.0},
                32: {'centre':161.3},
                }

bias_dict = {
    'ITMX': 100,
    'ITMY': -150,
    'ETMX': 100,
    'ETMY': 100
}
