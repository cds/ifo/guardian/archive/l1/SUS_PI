import time
import numpy as np

from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu
import signal
import subprocess
import os

# request upon initialization
request = 'IDLE'
# nominal operational state
nominal = 'MONITOR_LOCK'
actuationPermitted = False

import piconfig
# isc/<ifo>/guardian/ifoconfig.py
import ifoconfig

global process_dict
process_dict = {}
# which PLLs are active
pll_list = [8, 15, 16, 19, 24, 28, 32]
allModeList = list(range(1,33))
susList = ['ITMX', 'ITMY', 'ETMX', 'ETMY']

modesPerMirror = {}
for sus in susList:
    tempList = []
    for mode in piconfig.pi_list:
        if piconfig.pi_list[mode]['mirror'] == sus:
            tempList.append(mode)
    modesPerMirror[sus] = tempList

#################################################
# Functions
#################################################

def BPFilterPhaseEst(frequencyFromZero):
    '''
    The 10Hz wide band pass filters that the PI BP filters use (of the form cheby2("BandPass",4,40,middle-5,middle+5)butter("BandPass",6,middle-5,middle+5) )
    have a roughly linear phase response in a +/- 2 Hz, where the magnitude response is still, ~1. (mag falls fast off past that)
    This function gives you the approximate phase at some offset from the centre.

    Returns
    -------
    targetPhase: degrees shift accumilated from BP filter
    '''
    Phase = -371 - frequencyFromZero * 105
    return Phase

def extraBPFilterPhaseEst(frequencyFromZero):
    '''
    Some Modes reuqire an extra narrow bandpass. These unfortunate guys have other mirros having near exactly the same frequency mechanical modes.
    This compensates this ultra narrow filter, usually residing in the BP bank: FM10.
    
    The modes that this applies to live in the piconfig.extraBPmodes dict, which also provides the centre frequency of this BP, as it may vary.
    The Phase calculation here closely imitates:
    ellip("BandPass",6,1,40,frequencyFromZero-0.5,frequencyFromZero+0.5)
    and is only valid in that range (+/- 0.5 Hz) and has an error <10 Hz for that band.
    
    Usage of these may need to be reconsidered. Within 0.1 Hz span, you can expect about 60 degree change (so oscillation in +/- 0.5 Hz will be +/- 30 degree. This is why I actively improved frequency accuaracy as a function of amplitude in first step of the run loop.
    
    Returns
    -------
    targetPhase: degrees shift accumilated from BP filter
    '''
    Phase = (-360/(np.pi /1.021)*np.tan(frequencyFromZero * np.pi / (1.25)) -360) - 20*np.sin(np.pi*2.2*frequencyFromZero)
    return Phase

def findClosestIndex(inArray, targetValue):
    testArray = np.array(inArray)
    idx = (np.abs(testArray - targetValue)).argmin()
    return idx

def UpConvSIGcorr(frequencyFromDemod):
    '''
    The final very sharp filtering on the PI signals attenuates the lower sidband from the mixing between the upconversion frequency and the downconverted PI signal.
    This function approximates the amplitude response of this filter to be able to calculate amplitude limits in PLL damping

    Returns
    -------
    targetPhase: degrees shift accumilated from BP filter
    '''
    return -(1/500000) * (frequencyFromDemod) * (frequencyFromDemod-1000) +0.5

def dampMode(mode):
    '''
    This code block handles commencement of damping of any one mode through the raw path.
    
    It calculates the gain and phase and correct sign for damping the mode based on measured good damping paramters and the actual used bias settings for the mirror.
    '''
    refgain = piconfig.pi_list[mode]["damp_gain"]
    phase = piconfig.pi_list[mode]["damp_phase"]
    sus = piconfig.pi_list[mode]["mirror"]
    refbias = piconfig.pi_list[mode]["bias"]
    
    bias = piconfig.bias_dict[sus]
    gain = int(refgain * abs(refbias) / abs(bias))
    relBiasSign = 1 if ( np.sign(bias) == np.sign(refbias) ) else -1
    
    if actuationPermitted:
        if ifoconfig.lownoise_esd == sus:
            pass
        else:
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_TRAMP'] = 5 
            ezca.switch(f'SUS-{sus}_L3_LOCK_BIAS', "OFFSET", "ON") 
            time.sleep(0.1)
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_GAIN'] = 320
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET'] = bias

    # Caluculate correct damping gain
    ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','OFF','FM7','OFF','FM8','OFF','FM9','OFF','FM10','OFF')
    time.sleep(0.4)
    centralFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SET_FREQ']
    lockedFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_MON']
    # BP filters are 10 Hz wide, and in a region +/- 3 Hz, phase is approximately linear as following function
    BPPhase = BPFilterPhaseEst(lockedFrequency - centralFrequency)
    if mode in list(piconfig.extraBPmodes.keys()):
        BPPhase += extraBPFilterPhaseEst(lockedFrequency - piconfig[mode]["centre"])
    options = [ -BPPhase,
                -BPPhase +30,
                -BPPhase +60,
                -BPPhase +90,
                -BPPhase -180 -60,
                -BPPhase -180 -30,
                -BPPhase -180,
                -BPPhase -180 +30,
                -BPPhase -180 +60,
                -BPPhase -90,
                -BPPhase -60,
                -BPPhase -30
                ]
    caseInx = findClosestIndex(np.mod(options,360), phase)
    
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_TRAMP'] = 1
    # case switch on how to handle the phase
    if caseInx == 0: # 0
        FBphase = 1
    elif caseInx == 1: # +30
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM7','ON')
        FBphase = 1
    elif caseInx == 2: # +60
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON')
        FBphase = 1
    elif caseInx == 3: # +90 or -270
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON','FM7','ON')
        FBphase = 1
    elif caseInx == 4: # +120 or -240
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM9','ON')
        FBphase = -1
    elif caseInx == 5: # +150 or -210
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON')
        FBphase = -1
    elif caseInx == 6: # +180 or -180
        FBphase = -1
    elif caseInx == 7: # +210 or -150
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM7','ON')
        FBphase = -1
    elif caseInx == 8: # +240 or -120
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON')
        FBphase = -1
    elif caseInx == 9: # +270 or -90
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON','FM9','ON')
        FBphase = -1
    elif caseInx == 10: # -60
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM9','ON')
        FBphase = 1
    elif caseInx == 11: # -30
        ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON')
        FBphase = 1
    else:
        log('Failed to choose phase filter correctly')
        FBphase = 1 #catchall case to prevent errors in case of guardian error

    #Turn on ESD DRIVER
    if actuationPermitted:
        ezca[f'SUS-{sus}_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 1
    time.sleep(0.1)
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = relBiasSign * gain * FBphase

def dampModePLL(mode):
    '''
    This code block handles commencement of damping of any one mode, through the PLL path.
    
    It calculates the gain and phase and correct sign for damping the mode based on measured good damping paramters and the actual used bias settings for the mirror.
    '''
    refgain = piconfig.pi_list[mode]["damp_gain"]
    phase = piconfig.pi_list[mode]["damp_phase"]
    sus = piconfig.pi_list[mode]["mirror"]
    refbias = piconfig.pi_list[mode]["bias"]
    
    bias = piconfig.bias_dict[sus]
    gain = int(refgain * abs(refbias) / abs(bias))
    relBiasSign = 1 if ( np.sign(bias) == np.sign(refbias) ) else -1
    
    if actuationPermitted:
        if ifoconfig.lownoise_esd == sus:
            pass
        else:
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_TRAMP'] = 5 
            ezca.switch(f'SUS-{sus}_L3_LOCK_BIAS', "OFFSET", "ON") 
            time.sleep(0.1)
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_GAIN'] = 320
            ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET'] = bias
    
    ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','OFF','FM7','OFF','FM8','OFF','FM9','OFF','FM10','OFF')
    
    ## Caluculate correct damping gain
    ## !!! PLL Set Frequency must be central anticipated frequency of PI !!!
    time.sleep(0.2)
    centralFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SET_FREQ']
    lockedFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_MON']
    # BP filters are 10 Hz wide, and in a region +/- 3 Hz, phase is approximately linear as following function
    BPPhase = BPFilterPhaseEst(lockedFrequency - centralFrequency)
    if mode in list(piconfig.extraBPmodes.keys()):
        BPPhase += extraBPFilterPhaseEst(lockedFrequency - piconfig[mode]["centre"])
    
    # PLL phase creates a phase lag with positive phase, and phase lead with negative phase. Known bug as attested to by JoeB. This is the reason for the leading -1 below.
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_PHASE'] = -1*int(np.round(np.mod(phase-BPPhase,360)))
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_TRAMP'] = 1
    
    # Apply Amplitude limit to prevent DAC oversaturation DAC limit is 2**19, and the only real impact on this is the output SIG filter which attentuates the lower upconverted sideband.
    AmpLim = np.round(2**19 / (gain * UpConvSIGcorr(centralFrequency)), decimals=2)
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_OUT_LIMIT'] = AmpLim * 2.5 # Multiply by 2.5 here because, experimentally, thats how much less bandwidth a PLL can use, since less noise around the line gets pushed to the DAC
    ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_OUT', "LIMIT", "ON")
    time.sleep(0.1)
    
    # Turn on ESD DRIVER
    if actuationPermitted:
        ezca[f'SUS-{sus}_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 1
    time.sleep(0.1)
    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = relBiasSign * gain
    
def rePhase(mode):
    '''
    Re-evalaute damping phases of modes that have been damping for a long period of time.
    '''
    centralFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SET_FREQ']
    lockedFrequency = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_MON']
    BPPhase = BPFilterPhaseEst(lockedFrequency - centralFrequency)
    if mode in list(piconfig.extraBPmodes.keys()):
        BPPhase += extraBPFilterPhaseEst(lockedFrequency - piconfig[mode]["centre"])
    phase = piconfig.pi_list[mode]["damp_phase"]
    
    if mode in pll_list:
        # PLL phase creates a phase lag with positive phase, and phase lead with negative phase. This is the reason for the leading -1 below.
        ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_PHASE'] = -1*int(np.round(np.mod(phase-BPPhase,360)))
    else: # without PLL you have to recalculate EVERYTHING!
        refgain = piconfig.pi_list[mode]["damp_gain"]
        sus = piconfig.pi_list[mode]["mirror"]
        refbias = piconfig.pi_list[mode]["bias"]
        
        bias = piconfig.bias_dict[sus]
        gain = int(refgain * abs(refbias) / abs(bias))
        relBiasSign = 1 if ( np.sign(bias) == np.sign(refbias) ) else -1
        options = [ -BPPhase,
                    -BPPhase +30,
                    -BPPhase +60,
                    -BPPhase +90,
                    -BPPhase -180 -60,
                    -BPPhase -180 -30,
                    -BPPhase -180,
                    -BPPhase -180 +30,
                    -BPPhase -180 +60,
                    -BPPhase -90,
                    -BPPhase -60,
                    -BPPhase -30
                    ]
        caseInx = findClosestIndex(np.mod(options,360), phase)
        
        ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_TRAMP'] = 1
        # case switch on how to handle the phase
        if caseInx == 0: # 0
            FBphase = 1
        elif caseInx == 1: # +30
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM7','ON')
            FBphase = 1
        elif caseInx == 2: # +60
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON')
            FBphase = 1
        elif caseInx == 3: # +90 or -270
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON','FM7','ON')
            FBphase = 1
        elif caseInx == 4: # +120 or -240
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM9','ON')
            FBphase = -1
        elif caseInx == 5: # +150 or -210
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON')
            FBphase = -1
        elif caseInx == 6: # +180 or -180
            FBphase = -1
        elif caseInx == 7: # +210 or -150
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM7','ON')
            FBphase = -1
        elif caseInx == 8: # +240 or -120
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','ON')
            FBphase = -1
        elif caseInx == 9: # +270 or -90
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON','FM9','ON')
            FBphase = -1
        elif caseInx == 10: # -60
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM9','ON')
            FBphase = 1
        elif caseInx == 11: # -30
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM8','ON')
            FBphase = 1
        else:
            log('Failed to choose phase filter correctly')
            FBphase = 1 #catchall case to prevent errors in case of guardian error
        time.sleep(0.1)
        ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = relBiasSign * gain * FBphase
#################################################
# DECORATORS
#################################################



#################################################
# GUARDIAN STATES
#################################################


class INIT(GuardState):
    '''
    Initialise Line Tracking
    '''
    index = 0
    request = True

    def main(self):
        log('Initializing Parametric Instability Guardian:')
        return True


class IDLE(GuardState):
    index = 1

    def run(self):
        return True

class IFO_DOWN(GuardState):
    """
    Disable tracking and damping when the IFO unlocks. Turn off ITM bias.
    """
    index = 2
    def main(self):
        # Turn all output gains to 0
        for mode in allModeList:
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0
        # Turn off master output switch
        time.sleep(1) # Let the damping drives ramp down, to prevent kicking. Ramptimes default to 1.
        # Freset Filterbanks
        for mode in allModeList:
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP','FM6','OFF','FM7','OFF','FM8','OFF','FM9','OFF','FM10','OFF')
        for sus in susList:
            ezca[f'SUS-{sus}_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
        if actuationPermitted:
            for sus in ['ITMX', 'ITMY']: #ETMs handled by ISC LOCK
                ezca[f'SUS-{sus}_L3_LOCK_BIAS_TRAMP'] = 10
                time.sleep(0.1)
                ezca[f'SUS-{sus}_L3_LOCK_BIAS_GAIN'] = 320
                ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET'] = 0
                time.sleep(0.1)
                ezca.switch(f'SUS-{sus}_L3_LOCK_BIAS', "OFFSET", "OFF")
        for mode in allModeList:
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_OUT_LIMIT'] = 0
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_OUT', "LIMIT", "OFF")
        return True
    def run(self):
        return True

class MONITOR_LOCK(GuardState):
    index = 15
    
    def main(self):
        # The monitoring will only care about modes that have populated settings
        self.complete_config = []
        for mode in allModeList:
            if all(piconfig.pi_list[mode].values()):
                self.complete_config.append(mode)
        
        for mode in list(piconfig.extraBPmodes.keys()): # Make sure Extra BP filter engaged for modes with *very* close neighbors
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_BP','FM10','ON')
        # Reset PLL frequency integrator at the start of monitoring.
        for mode in allModeList:
            # This list of PLL settings should just always work, and only the filter2 reset should be necessary if PLL locks to a wrong line. This is supported in the monitor code.
            # This actually takes about 2 minutes...
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT2_RSET'] = 2
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT2','FM1','ON','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1_GAIN'] = 0.5
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1','FM1','ON','FM2','ON','FM3','OFF', 'FM4','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1','FM1','ON','FM2','OFF','FM3','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_FILT','FM1','OFF','FM2','ON','FM3','OFF','FM4','OFF','FM5','OFF')
            # ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_OUT','FM1','OFF','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_I','FM1','OFF','FM2','OFF','FM3','OFF','FM4','ON','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_Q','FM1','ON','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_LOCK','FM1','OFF','FM2','ON','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_THETA','FM1','OFF','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SIGNAL','FM1','OFF','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
            ezca.switch(f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_COUNT','FM1','ON','FM2','OFF','FM3','OFF','FM4','OFF','FM5','OFF')
        for mode in allModeList:
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_IWAVE_PLL_SEL_MTRX_1_2'] = 0 #never use IWAVE. It is broken
            if mode not in pll_list:
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_IWAVE_PLL_SEL_MTRX_1_1'] = 0
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_IWAVE_PLL_SEL_MTRX_1_3'] = 1
            else:
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_IWAVE_PLL_SEL_MTRX_1_1'] = 1
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_IWAVE_PLL_SEL_MTRX_1_3'] = 0
        # Despite most modes not using PLL, PLL should still be locking (and usable really, just needs testing), PLL can be used to trigger damping, and is needed to know the phase of the damping.
        # To make this work, inputs to PLLs are to be normalised around 1, indicating the 'noise' level, and if PLL output is any larger, then the line observed is of some SNR, and we can trigger on SNR.
        # To get the most out of the PLLs the SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1_GAIN needs to be reduced from 3 to ~0.3 and FM1 engaged. This reduces noise bandwidth,
        # together with the always engaged filter on the Amplitude (which can be limited to not saturate DAC).
        
        self.targetbias = {}
        for sus in susList:
            if not ifoconfig.lownoise_esd == sus:
                self.targetbias[sus] = piconfig.bias_dict[sus]
            else:
                self.targetbias[sus] = ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET']
        
        self.nominalbias = {} 
        for sus in susList:
            if not ifoconfig.lownoise_esd == sus:
                self.nominalbias[sus] = 0
            else:
                self.nominalbias[sus] = ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET']
        self.doNotDampList = []
        
        ##Temporary:
        #ezca['SUS-PI_PROC_COMPUTE_MODE15_BP_GAIN'] = 0
        return True
    
    def run(self):
        '''
        Damping strategy, circa VB 20230922:
            1.0) Wait on PLL Lock signal threshold [or a low amplitude threshold]
            1.1) Is PLL frequency within 1.5 Hz of centre? Reset pll frequency integrator if not.
            2.0) Wait on high amplitude threshold to trigger damping
            2.1) Apply relevant damping gain/phase. Continuously reevaluate phase.
            Anytime) PLL loses lock (either frequency rapidly drifts due to noise or Amplitude too low):    Disengage PLL lock and any present damping, return to top after short delay.
        '''
        # Temorary disable damping
        # return True
        
        # Below block not helpful. Comment out VB 20230929
        # # Tighten up the frequency filter based on the amplitude. This reduces error in frequency estimates
        # for mode in self.complete_config: # should be allModeList, but this spams the guardian too heavily.
            # amp = int(ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_FILT_OUTPUT'])
            # if amp < 5:
                # pllFreqFollowGain = 0.5
            # elif 5 < amp < 10:
                # pllFreqFollowGain = int(10 - amp) / 10
            # elif amp > 10:
                # pllFreqFollowGain = 0.1
            # else:
                # pllFreqFollowGain = 0.5 #catchall case to prevent errors in case of guardian error
            # ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1_GAIN'] = pllFreqFollowGain
        
        # Check PLL status. Reset integrator if frequency drifts.
        for mode in self.complete_config:
            freqDiff = abs(ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_MON'] - ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SET_FREQ'])
            if (freqDiff > 1.5):
                log(f'Mode {mode} frequency has drifted beyond 1.5 Hz of the centre. Resetting Frequency integrator.')
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT2_RSET'] = 2
        
        # Refresh list of modes currently Damping
        currentlyDamping = []
        for mode in self.complete_config:
            if abs(ezca[f"SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN"]) > 1e-5: #dont rely on 0, as it may be an imprecise float
                currentlyDamping.append(mode)
                rePhase(mode)
        time.sleep(1) # to prevent extremely rapid phase reapplication
        
        # For modes not damping, 
        currentlyNOTdamping = set(self.complete_config) - set(currentlyDamping)
        for mode in currentlyNOTdamping:
            if mode not in self.doNotDampList:
                # amplitude = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_FILT_OUTPUT']
                amplitude = ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_RMSMON']
                freqDiff = abs(ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_MON'] - ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_SET_FREQ'])
                if amplitude > 9: 
                    # this handles merely engaging the damping, but not turning it off
                    log(f'Damping Mode {mode}')
                    if mode in pll_list:
                        dampModePLL(mode)
                    else:
                        dampMode(mode)
                    time.sleep(1)
                    return True
                elif (ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_FILT_OUTPUT'] < 3) and (amplitude > 5):
                    # This might occur if we are not locked on the right peak somehow.
                    log(f'RMS amplitude is high but PLL amplitue is low. for Mode {mode} Resetting Frequency integrator.')
                    ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT2_RSET'] = 2
                    return True
                else:
                    pass
        
        # The below is executed once any mode "Completes" damping.
        for mode in currentlyDamping:
            # if ezca[f"SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_AMP_FILT_OUTPUT"] < 3:
            if ezca[f"SUS-PI_PROC_COMPUTE_MODE{mode}_RMSMON"] < 6:
                # If amplitude is small, cease damping
                log(f'Halting Damping of Mode {mode}')
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT2_RSET'] = 2
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_PLL_FREQ_FILT1_GAIN'] = 0.5
                currentlyDamping.remove(mode)
                time.sleep(0.5)
                # If there are no actively damping modes on QUAD (sus), then return bias to nominal
                if actuationPermitted:
                    for sus in susList:
                        if not any(i in currentlyDamping for i in modesPerMirror[sus]):
                            log(f'No more modes damping on suspension: {sus}. Returning bias to nominal.')
                            ezca[f'SUS-{sus}_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
                            if not ifoconfig.lownoise_esd == sus: # Don't want to risk doing anything to the locking ESD.
                                ezca[f'SUS-{sus}_L3_LOCK_BIAS_TRAMP'] = 10
                                time.sleep(0.1)
                                ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET'] = self.nominalbias[sus]
                                if sus in ['ITMX', 'ITMY']:
                                    ezca.switch(f'SUS-{sus}_L3_LOCK_BIAS', "OFFSET", "OFF")
                return True
            # If gain/phase is incorrect you will drive the mode up. This is an attempt at a failsafe:
            elif ezca[f"SUS-PI_PROC_COMPUTE_MODE{mode}_RMSMON"] > 1000 and mode not in self.doNotDampList:
                self.doNotDampList.append(mode)
                currentlyDamping.remove(mode)
                log(f'Halting Damping of Mode {mode} due to excess amplitude, which we may be inducing.')
                ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0
                time.sleep(0.5)
                # If there are no actively damping modes on QUAD (sus), then return bias to nominal
                if actuationPermitted:
                    for sus in susList:
                        if not any(i in currentlyDamping for i in modesPerMirror[sus]):
                            log(f'No more modes damping on suspension: {sus}. Returning bias to nominal.')
                            ezca[f'SUS-{sus}_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
                            if not ifoconfig.lownoise_esd == sus: # Don't want to risk doing anything to the locking ESD.
                                ezca[f'SUS-{sus}_L3_LOCK_BIAS_TRAMP'] = 10
                                time.sleep(0.1)
                                ezca[f'SUS-{sus}_L3_LOCK_BIAS_OFFSET'] = self.nominalbias[sus]
                                if sus in ['ITMX', 'ITMY']:
                                    ezca.switch(f'SUS-{sus}_L3_LOCK_BIAS', "OFFSET", "OFF")
                return True
            notify('Currnetly damping modes:'+' '.join(map(str,currentlyDamping)))
        return True


edges = [
    ('INIT', 'IDLE'),
    ('IFO_DOWN', 'IDLE'),
    ('IDLE', 'MONITOR_LOCK'),
    ('MONITOR_LOCK', 'IFO_DOWN')
]
